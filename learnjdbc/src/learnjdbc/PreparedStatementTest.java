package learnjdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class PreparedStatementTest {
	
	private static final String SEARCH_EMP_BY_NAME = "select * from EmployeeDetails where FullName like ?";
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter name to search ");
		String name = sc.nextLine();
		Connection cnx = null;
		try {
			cnx = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/mydb",
					"javauser",
					"javapassword");
			PreparedStatement pStmt = cnx.prepareStatement(SEARCH_EMP_BY_NAME);
			pStmt.clearParameters();
			pStmt.setString(1, "%" + name + "%");
			pStmt.execute();
			ResultSet rs = pStmt.getResultSet();
			while (rs.next()) 
				System.out.println(new EmployeeDetail(rs));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			try {
				cnx.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
