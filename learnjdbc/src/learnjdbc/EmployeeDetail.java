package learnjdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class EmployeeDetail {
	
	private int empId;
	private String fullName;
	private int managerId;
	private Date dateOfJoining;
	private String city;
	
	public EmployeeDetail(ResultSet rs) throws SQLException {
		empId = rs.getInt("EmpId");
		fullName = rs.getString("FullName");
		managerId = rs.getInt("ManagerId");
		dateOfJoining = rs.getDate("DateOfJoining");
		city = rs.getString("City");
	}

	@Override
	public String toString() {
		return "EmployeeDetail [empId=" + empId + ", fullName=" + fullName + ", managerId=" + managerId
				+ ", dateOfJoining=" + dateOfJoining + ", city=" + city + "]";
	}

}
