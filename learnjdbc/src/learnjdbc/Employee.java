package learnjdbc;

public class Employee {
	
	private int id;
	private String name;
	private float salary;
	private String group;
	
	public Employee (int id, String name, float salary, String group) {
		this.id = id;
		this.name = name;
		this.salary = salary;
		this.group = group;
	}
	
	public float getSalary() {
		return salary;
	}
	
	public void setSalary(float salary) {
		this.salary = salary;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", salary=" + salary + ", group=" + group + "]";
	}

}
