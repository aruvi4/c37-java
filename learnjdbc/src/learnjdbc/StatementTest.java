package learnjdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class StatementTest {
	
	private static final String EMPLOYEES = "EmployeeDetails";
	private static final String SELECT_ALL_EMPLOYEES = "select * from " + EMPLOYEES + ";";
	
	public static void main (String[] args) {
		Connection cnx = null;
		
		try {
			cnx = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/mydb",
					"javauser",
					"javapassword");
			System.out.println(cnx.getMetaData().getDriverName());
			Statement stmt = cnx.createStatement();
			stmt.execute(SELECT_ALL_EMPLOYEES);
			ResultSet rs = stmt.getResultSet();
			while (rs.next()) {
				EmployeeDetail ed = new EmployeeDetail(rs);
				System.out.println(ed);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			try {
				cnx.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
