
public class StringsFun {
	
	public static String reverse (String input) {
		String reversed = "";
		for (int i = input.length() - 1; i >= 0; i--)
			reversed += input.charAt(i);
		return reversed;
	}
	
	public static boolean isPalindrome(String input) {
		return reverse(input).equals(input);
	}
	
	public static void swap (char[] arr, int i, int j) {
		char temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}
	
	public static void sort (char[] arr) {
		boolean haveSwapped = true;
		while (haveSwapped) {
			haveSwapped = false;
			for (int i = 0; i < arr.length - 1; i++)
				if (arr[i] > arr[i + 1]) {
					swap(arr, i, i + 1);
					haveSwapped = true;
				}
		}
	}
	
	public static String sorted (String input) {
		char[] asChars = input.toCharArray();
		sort(asChars);
		return String.valueOf(asChars);
	}
	
	public static boolean areAnagrams (String str1, String str2) {
		return sorted(str1).equals(sorted(str2));
	}
	
	public static String wordWiseReverse (String input) {
		String[] words = input.split(" ");
		String output = "";
		for (int i = 0; i < words.length; i++)
			output += reverse(words[i]) + " ";
		return output;
	}
	
	public static String rot13 (String input) {
		String alphas = "abcdefghijklmnopqrstuvwxyzabcdefghijklmn";
		String output = "";
		for (int i = 0; i < input.length(); i++) 
			output += alphas.charAt(alphas.indexOf(input.charAt(i)) + 13);
		return output;
	}
	
	public static boolean isVowel (char x) {
		String vowels = "aeiouAEIOU";
		return vowels.contains(String.valueOf(x));
	}
	
	public static boolean isConsonant (char x) {
		String consonants = "bcdfghjklmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ";
		return consonants.contains(String.valueOf(x));
				
	}
	
	public static int[] numVowelsAndConsonants (String input) {
		int numVowels = 0;
		int numConsonants = 0;
		for (int i = 0; i < input.length(); i++) {
			if (isVowel(input.charAt(i)))
				numVowels++;
			if (isConsonant(input.charAt(i)))
				numConsonants++;
		}
		int[] solution = {numVowels, numConsonants};
		return solution;
	}
	
	public static int secondsFromMidnight(String ts) {
		String[] stamps = ts.split(":");
		int hours = Integer.valueOf(stamps[0]);
		int mins = Integer.valueOf(stamps[1]);
		int secs = Integer.valueOf(stamps[2]);
		return hours * 3600 + mins * 60 + secs;
	}
	
	public static int timeDifference(String ts1, String ts2) {
		return Math.abs(secondsFromMidnight(ts2) - secondsFromMidnight(ts1));
	}
	
	public static int strictTimeDifference(String ts1, String ts2) {
		int timeDiff = secondsFromMidnight(ts2) - secondsFromMidnight(ts1);
		if (timeDiff < 0)
			timeDiff += 24 * 3600;
		return timeDiff;
	}
	
	public static int score (char x) {
		String alphas = " abcdefghijklmnopqrstuvwxyz";
		return alphas.indexOf(x);
	}
	
	public static int scoreString (String input) {
		char[] chars = input.toCharArray();
		int sum = 0;
		for (char x : chars)
			sum += score(x);
		return sum;
	}
	
	public static void main (String[] args) {
		System.out.println(scoreString("talent"));
	}

}
