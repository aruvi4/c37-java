
public class TemperatureConverter {
	
	/*DRY - DON'T REPEAT YOURSELF*/
	
	/* MISTAKES WE MADE
	 * 1. Writing everything in main()
	 * 2. Taking more than 1 parameter to our conversion method
	 * 3. 
	 * 4. Unnecessary temporary variable inside our conversion method
	 * 5. Variables started with uppercase character; 
	 *    class didn't start with uppercase character
	 * */
	
	public static final boolean C_TO_F = true;
	public static final boolean F_TO_C = false;
	
	public static float toFahrenheit (float celsius) {
		return celsius * 9f / 5f + 32f;
	}
	
	public static float toCelsius (float fahrenheit) {
		return (fahrenheit - 32f) * 5f / 9f;
	}
	
	public static float convert(float temp, boolean conversionCode) {
		/* true: Celsius to Fahrenheit
		 * false: Fahrenheit to Celsius
		 */
		if (conversionCode == C_TO_F) {
			return toFahrenheit(temp);
		}
		else {
			return toCelsius(temp);
		}
		
	}
	
	public static boolean areProportional(int larger, int smaller) {
		return larger % smaller == 0;
	}
	
	public static boolean canVote(int age) {
		return age >= 18;
	}
	
	public static void main(String[] args) {
		float celsiusTemp = 37f;
		System.out.println(toFahrenheit(celsiusTemp));
		System.out.println(toCelsius(98.6f));
	}

}
