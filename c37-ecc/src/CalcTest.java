import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CalcTest {
	
	private final Calc c = new Calc();

	@Test
	void test() {
		int sum = c.add(1, 2);
		assertEquals(3, sum);
	}

}
