package training;

public class Dog {
	
	static String speciesName = "Canis Familaris";
	
	String name;
	
	public static void main (String[] args) {
		Dog d1 = new Dog();
		d1.name = "Jimmy";
		
		Dog d2 = new Dog();
		d2.name = "Tommy";
		
		System.out.println(Dog.speciesName);
	}

}
