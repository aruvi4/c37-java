
public class Loops {
	
	public static String printUpto (int limit) {
		String output = "";
		int current = 0;
		while (current < limit) {
			output += current + "\n";
			current++;
		}
		return output;
	}
	
	public static String printUptoDesc (int limit) {
		String output = "";
		int current = limit - 1;
		while (current >= 0) 
			output += current-- + "\n";
		
		return output;
	}
	
	public static int sumOfDigits(int num) {
		int sum = 0;
		while (num > 0) {
			sum += num % 10;
			num /= 10;
		}
		return sum;
	}
	public static int numDigits (int num) {
		if (num == 0) 
			return 1;
		if (num < 0)
			return numDigits(-num);
		int numDigits = 0;
		while (num > 0) {
			numDigits++;
			num /= 10;
		}
		return numDigits;
	}
	
	public static int numDigitsLen(int num) {
		int length = String.valueOf(num).length();
		//Syntax of the ternary operator
		//instead of writing
		//if (expr1)
		//    expr2
		//else
		//    expr3
		//write
		// (expr1) ? expr2 : expr 3
		return (num >= 0) ? length : length - 1;
	}
	
	public static String printEvenUpto (int limit) {
		String output = "";
		int current = 0;
		while (current < limit) {
			output += current + "\n";
			current += 2;
		}
		return output;
	}
	public static String printEvenUptoB (int limit) {
		String output = "";
		int current = 0;
		while (current < limit) {
			if (current % 2 == 0)
				output += current + "\n";
			current++;
		}
		return output;
	}
	
	public static String printEvenUptoC (int limit) {
		String output = "";
		for (int current = 0; current < limit; current++)
			if (current % 2 == 0)
				output += current + "\n";
		return output;
	}
	
	public static void main(String[] args) {
		System.out.println(numDigitsLen(-378));
	}

}
