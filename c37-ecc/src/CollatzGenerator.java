
public class CollatzGenerator {
	
	public static final String SEPARATOR = ", ";
	
	public static int nextCollatz(int current) {
		if (current % 2 == 0)
			return current / 2;
		else
			return current * 3 + 1;
	}
	
	public static String collatz (int start) {
		if (start == 4)
			return 4 + SEPARATOR + 2 + SEPARATOR + 1;
		return start + SEPARATOR + collatz(nextCollatz(start));
	}
	
	public static String collatzLoop (int start) {
		String sequence = "" + start;
		while (start != 4) {
			start = nextCollatz(start);
			sequence += SEPARATOR;
			sequence += start;
		}
		sequence += SEPARATOR + 2 + SEPARATOR + 1;
		return sequence;
	}
	
	public static String collatzForLoop (int start) {
		String sequence = "" + start + SEPARATOR;
		for (int current = nextCollatz(start); current != 4; current = nextCollatz(current))
			sequence += current + SEPARATOR;
		return sequence;
	}
	
	public static void main(String []args) {
		System.out.println(collatzForLoop(23));
	}

}
