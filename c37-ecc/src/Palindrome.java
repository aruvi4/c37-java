
public class Palindrome {
	
	/*
	 * Write a java method that takes one input, a three-digit integer,
	 * and returns true if the number is a palindrome
	 * and false otherwise
	 */
	public static boolean is3DigitPalindrome(int num) {
		return (num / 100 == num % 10);
	}
	
	public static boolean shouldRoundUp(int num) {
		boolean roundUp = true;
		if (num % 10 == 5) 
			if (((num / 10) % 10) % 2 == 0) 
				roundUp = false;
			else roundUp = true;
		else
			if (num % 10 > 5)
				roundUp = true;
			else
				roundUp = false;
		return roundUp;
	}
	
	public static int nearest10(int num) {		
		int withoutUnit = num / 10;
		if (shouldRoundUp(num))
			return (withoutUnit + 1) * 10;
		else
			return withoutUnit * 10;
	}
	
	public static void main (String[] args) {
		
	}

}
