package module2;

public class Deck {
	
	//not an IS-A relationship
	//HAS-A relationship
	private Card[] cards;
	
	public Deck () throws InvalidRankException, InvalidSuitException {
		cards = new Card[52];
		int k = 0;
		for (int i = 1; i <= 13; i++)
			for (int j = 1; j <= 4; j++)
				cards[k++] = new Card(j, i);
	}
	
	public String toString() {
		String output = "[";
		String separator = ", ";
		for (Card c : cards)
			output += c + separator;
		return output + "]";
	}
	
	public void swap (int i, int j) {
		Card temp = cards[i];
		cards[i] = cards[j];
		cards[j] = temp;
	}
	
	public void shuffle() {
		int numSwaps = 100; //you can make this more or less
		while (numSwaps-- > 0) {
			int i = (int)(Math.random() * 100) % 52; //generate a random number here
			int j = (int)(Math.random() * 100) % 52; //generate a random number here
			swap(i, j);
		}
	}
	
	public Card remove(int index) {
		Card c = cards[index];
		Card[] temp = new Card[cards.length - 1];
		int j = 0;
		for (int i = 0; i < cards.length; i++)
			if (i != index)
				temp[j++] = cards[i];
		cards = temp;
		return c;
	}

}
