package module2.exceptions;

public class InsufficientBalanceException extends Exception {
	
	public InsufficientBalanceException (String message) {
		super(message);
	}

}
