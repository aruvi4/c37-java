package module2;

public class Student extends Person{
	
	public Student(String name, int age, String gender, double height, double weight) {
		super(name, age, gender, height, weight);
	}
	
	@Override
	public String getSleepTime() {
		return "12am";
	}
}
