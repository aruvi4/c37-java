package module2;

import module2.exceptions.InsufficientBalanceException;

public class MainTestClass {
	
	public static int doubleNum (int a) {
		if (a <= 0)
			throw new ArithmeticException("invalid");
		return 2 * a;
	}
	
	public static void main (String[] args) {
		SavingsAccount a = new SavingsAccount("aruvi", "hyderabad" , 0);
		try {
			a.withdraw(5000);
		} catch (InsufficientBalanceException e) {
			e.printStackTrace();
		}
		int x = 5;//primitive
		Integer y = 5;//wrapper
		Integer z = Integer.valueOf(x);//converting primitive to wrapper
	}

}
