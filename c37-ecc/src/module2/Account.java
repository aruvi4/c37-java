package module2;

public class Account {
	
	private static int accNumGenerator = 101;
	private String name;
	private String address;
	private int accountNumber;
	protected double balance;
	
	public Account(String name, String address, double balance) {
		this.name = name;
		this.address = address;
		this.accountNumber = accNumGenerator++;
		this.balance = balance;
	}
	
	public Account(String name, String address) {
		this.name = name;
		this.address = address;
		this.accountNumber = accNumGenerator++;
		this.balance = 0;
	}

}
