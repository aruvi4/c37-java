package module2.collections;

public class ThirdStandardStudent {
	
	private String name;
	private ThirdStandardStudent next;
	
	public ThirdStandardStudent(String name) {
		this.name = name;
		this.next = null;
	}
	
	public ThirdStandardStudent(ThirdStandardStudent copy) {
		this.name = copy.name;
		this.next = copy.next;
	}
	
	public ThirdStandardStudent getNext() {
		return next;
	}
	
	public void setNext(ThirdStandardStudent s) {
		this.next = s;
	}

}
