package module2.collections;

public class Product {
	
	private String name;
	private int id;
	private double price;
	private Category category;
	
	private static int idgen = 101;
	
	public Product(String name, double price, Category category) {
		this.name = name;
		this.price = price;
		this.category = category;
		this.id = idgen++;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
	
	public int getID() {
		return id;
	}

	@Override
	public String toString() {
		return "Product [name=" + name + ", id=" + id + ", price=" + price + ", category=" + category + "]";
	}
	
	

}
