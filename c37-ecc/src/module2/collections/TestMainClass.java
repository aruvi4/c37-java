package module2.collections;

import java.util.List;

public class TestMainClass {
	
	public static MapWarehouse populate() {
		Category c1 = new Category("headphones", 1);
		Product p1 = new Product("bose", 10000, c1);
		Product p2 = new Product("boat", 2500, c1);
		
		Category c2 = new Category("mobiles", 2);
		Product p3 = new Product("mi", 15000, c2);
		Product p4 = new Product("iphone", 50000, c2);
		
		MapWarehouse w = new MapWarehouse();
		w.add(p1);
		w.add(p2);
		w.add(p3);
		w.add(p4);
		
		return w;
	}
	
	public static void main(String[] args) {
		MapWarehouse w = populate();
		
		List<Product> headphones = w.getProductsOf(new Category("headphones", 1));
		
		if (headphones != null)
			for (Product p : headphones)
				System.out.println(p);
		else 
			System.out.println("no products found");
	}

}
