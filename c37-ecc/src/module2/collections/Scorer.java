package module2.collections;

import java.util.HashMap;
import java.util.Map;

public class Scorer {
	
	private Map<String, Integer> scoreMap;
	
	public Scorer() {
		scoreMap = new HashMap<String, Integer> ();
		scoreMap.put("aeiouAEIOU", 1);
		scoreMap.put("vwxyzVWXYZ", 2);
		scoreMap.put("bcdfgBCDFG", 2);
		scoreMap.put("hjklmHJKLM", 3);
		scoreMap.put("npqrstNPQRST", 3);
	}
	
	public int score(char c) {
		for (String sequence : scoreMap.keySet())
			if (sequence.contains(String.valueOf(c)))
				return scoreMap.get(sequence);
		return 0;
	}
	
	public int score(String s) {
		int total = 0;
		for (char c : s.toCharArray())
			total += score(c);
		return total;
	}
	
	public static void main(String []args) {
		Scorer s = new Scorer();
		System.out.println(s.score("helloworld"));
	}

}
