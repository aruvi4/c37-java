package module2.collections;

public class Student {
	
	private int id;
	private int mathMarks;
	
	public int getMathMarks() {
		return mathMarks;
	}
	
	public Student(int id, int mathMarks) {
		this.id = id;
		this.mathMarks = mathMarks;
	}
	
	@Override
	public String toString() {
		return "ID: " + this.id + "Math marks: " + this.mathMarks; 
	}

}
