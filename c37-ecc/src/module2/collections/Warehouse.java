package module2.collections;

import java.util.ArrayList;
import java.util.List;

public class Warehouse {
	
	private List<Product> products;
	
	public Warehouse() {
		this.products = new ArrayList<Product> ();
	}
	
	public void add(Product p) {
		this.products.add(p);
	}
	
	public List<Product> getProductsOf(Category c) {
		List<Product> output = new ArrayList<Product> ();
		
		for (Product p : products)
			if (p.getCategory().equals(c))
				output.add(p);
		
		return output;
	}

}
