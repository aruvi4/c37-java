package module2.collections;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapWarehouse {
	
	private Map<Category, List<Product>> products;
	
	public MapWarehouse() {
		products = new HashMap<Category, List<Product>> ();
	}
	
	public void add(Product p) {
		if (products.containsKey(p.getCategory()))
			products.get(p.getCategory()).add(p);
		else {
			ArrayList<Product> temp = new ArrayList<Product> ();
			temp.add(p);
			products.put(p.getCategory(), temp);
		}
	}
	
	public List<Product> getProductsOf(Category c) {
		return products.get(c);
	}

}
