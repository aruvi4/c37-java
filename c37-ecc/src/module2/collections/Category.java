package module2.collections;

public class Category {
	
	private String name;
	private int cid;
	
	public Category(String name, int cid) {
		this.name = name;
		this.cid = cid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void setCid(int cid) {
		this.cid = cid;
	}

	public int getCid() {
		return cid;
	}

	@Override
	public String toString() {
		return "Category [cid=" + cid + ", name=" + name + "]";
	}
	
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Category))
			return false;
		Category temp = (Category) other;
		return temp.name.equalsIgnoreCase(this.name);
	}
	
	@Override
	public int hashCode() {
		return cid;
	}
	

}
