package module2.collections;
import java.util.ArrayList;
import java.util.List;

public class ArrayListDemo {
	
	public static double classAverage(List<Student> students) {
		double classAverage;
		double sum = 0;
		for (Student student : students) {
			sum += student.getMathMarks();
		}
		classAverage = sum / students.size();
		return classAverage;
	}
	
	public static void main(String[] args) {
		List<Student> students = new ArrayList<Student>();
		Student s1 = new Student(1, 100);
		Student s2 = new Student(2, 40);
		Student s3 = new Student(3, 75);
		Student s4 = new Student(4, 60);
		students.add(s1);
		students.add(s2);
		students.add(s3);
		students.add(s4);
		
		System.out.println(classAverage(students));
		
		students.remove(s1);
		
		System.out.println(classAverage(students));
	}

}
