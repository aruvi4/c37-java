package module2.collections;

public class ThirdStandardStudentList {
	
	ThirdStandardStudent first;
	ThirdStandardStudent last;
	
	public void append(ThirdStandardStudent s) {
		
		last.setNext(s);
	}
	
	public ThirdStandardStudent get(int index) {
		ThirdStandardStudent temp = new ThirdStandardStudent(first);
		for (int i = 0; i < index - 1; i++)
			temp = temp.getNext();
		return temp;
	}
}

/*
 * adding an element 1000 times and searching for an element 1 time.
 * ArrayList ->
 * 1 + 2 +3 + 4 +... + 1000 + 1
 * LinkedList ->
 * 1 + 1 + 1 + .. + 1 + 1000
*/