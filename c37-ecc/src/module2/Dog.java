package module2;

public class Dog {
	
	private String name;
	private String breed;
	private int age;
	private String color;
	
	public Dog (String name, String breed, int age, String color) {
		this.name = name;
		this.breed = breed;
		this.age = age;
		this.color = color;
	}
	
	public String bark() {
		return "woof";
	}
	
	public String toString() {
		return color + " Dog: " + name + " is of breed " + breed + " and is " + age + " years old";
	}

}
