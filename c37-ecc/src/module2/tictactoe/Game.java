package module2.tictactoe;
import java.util.Scanner;

public class Game {
	
	public static void main(String[] args) {
		Board b = new Board();
		System.out.println(b);
		Scanner sc = new Scanner(System.in);
		int winner = -1;
		Player X = new Player("X");
		Player O = new Player("O");
		while(true) {
			System.out.println("X's turn. Enter row(0-2) followed by column(0-2) to place ");
			int row = sc.nextInt();
			int column = sc.nextInt();
			b.mark(row, column, X.getChosenValue());
			System.out.println(b);
			if (b.winner() != -1) {
				winner = b.winner();
				break;
			}
			System.out.println("O's turn. Enter row(0-2) followed by column(0-2) to place ");
			row = sc.nextInt();
			column = sc.nextInt();
			b.mark(row, column, O.getChosenValue());
			System.out.println(b);
			if (b.winner() != -1) {
				winner = b.winner();
				break;
			}
		}
		if (winner == X.getChosenValue())
			System.out.println("X won!");
		if (winner == O.getChosenValue())
			System.out.println("O won!");
	}

}
