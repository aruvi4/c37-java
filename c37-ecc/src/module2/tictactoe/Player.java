package module2.tictactoe;

public class Player {
	
	private int chosenValue;
	
	public Player(String chosenLetter) {
		if (chosenLetter.equalsIgnoreCase("X"))
			chosenValue = Cell.X;
		if (chosenLetter.equalsIgnoreCase("O"))
			chosenValue = Cell.O;
	}
	
	public int getChosenValue() {
		return chosenValue;
	}

}
