package module2.tictactoe;

public class Cell {
	
	public static final int X = 1;
	public static final int O = 0;
	public static final int BLANK = -1;
	
	private int value;
	
	public Cell() {
		this.value = BLANK;
	}
	
	public void setX() {
		this.value = X;
	}
	
	public void setO() {
		this.value = O;
	}
	
	public int getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		String printableValue = "";
		if (value == X)
			printableValue += "X";
		if (value == O)
			printableValue += "O";
		if (value == BLANK)
			printableValue += "_";
		return "|" + printableValue + "|";
	}
	
	@Override
	public boolean equals(Object other) {
		Cell castOther = (Cell) other;
		if (this.value == BLANK || castOther.value == BLANK)
			return false;
		return this.value == castOther.value;
	}

}
