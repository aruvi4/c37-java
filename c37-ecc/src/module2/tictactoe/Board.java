package module2.tictactoe;

import java.util.ArrayList;
import java.util.List;

public class Board {
	
	private List<List<Cell>> cells;
	
	public Board() {
		cells = new ArrayList<List<Cell>>();
		
		for (int i = 0; i < 3; i++) {
			List<Cell> row = new ArrayList<> ();
			for (int j = 0; j < 3; j++)
				row.add(new Cell());
			cells.add(row);
		}
	}
	
	public String toString() {
		String boardRepr = "";
		for (List<Cell> row : cells) {
			for (Cell cell : row) {
				boardRepr += cell;
			}
			boardRepr += "\n";
		}
		return boardRepr;
	}
	
	public void mark (int row, int column, int value) {
		if (value == Cell.X)
			cells.get(row).get(column).setX();
		if (value == Cell.O)
			cells.get(row).get(column).setO();
	}
	
	public int winner() {
		//check rows
		for (int i = 0; i < 3; i++)
			if (cells.get(i).get(0).equals(cells.get(i).get(1))
				&&
				cells.get(i).get(1).equals(cells.get(i).get(2)))
				return cells.get(0).get(0).getValue();
		//check columns
		for (int i = 0; i < 3; i++)
			if (cells.get(0).get(i).equals(cells.get(1).get(i))
			&&
			cells.get(1).get(i).equals(cells.get(2).get(i)))
			return cells.get(0).get(0).getValue();
		//check negative diagonal
		if (cells.get(0).get(0).equals(cells.get(1).get(1))
				&&
				cells.get(1).get(1).equals(cells.get(1).get(1)))
				return cells.get(0).get(0).getValue();
		//check positive diagonal
		if (cells.get(2).get(0).equals(cells.get(1).get(1))
				&&
				cells.get(1).get(1).equals(cells.get(0).get(2)))
				return cells.get(0).get(0).getValue();
		return -1;
	}

}
