package module2;

public class Card implements Comparable<Card>{
	
	private int suit;
	private int rank;
	
	public static final int SPADES = 1;
	public static final int HEARTS = 2;
	public static final int DIAMONDS = 3;
	public static final int CLUBS = 4;
	
	public static final int ACE = 1;
	public static final int TWO = 2;
	public static final int THREE = 3;
	public static final int FOUR = 4;
	public static final int FIVE = 5;
	public static final int SIX = 6;
	public static final int SEVEN = 7;
	public static final int EIGHT = 8;
	public static final int NINE = 9;
	public static final int TEN = 10;
	public static final int JACK = 11;
	public static final int QUEEN = 12;
	public static final int KING = 13;
	
	private static final String[] SUITS = {"", "Spades", "Hearts", 
			"Diamonds", "Clubs"};
	private static final String[] RANKS = {"", "Ace", "Two", 
			"Three", "Four", "Five", "Six", "Seven", 
			"Eight", "Nine", "Ten", "Jack", "Queen", "King"};
	
	public static final String RED = "Red";
	public static final String BLACK = "Black";
	
	
	public Card (int suit, int rank) throws InvalidRankException, InvalidSuitException{
		setRank(rank);
		setSuit(suit);
	}

	public int getSuit() {
		return suit;
	}

	public void setSuit(int suit) throws InvalidSuitException{
		if (suit > CLUBS || suit < SPADES) {
			throw new InvalidSuitException("invalid suit");
		}
		this.suit = suit;
	}

	public int getRank() {
		
		return rank;
	}

	public void setRank(int rank) throws InvalidRankException{
		if (rank > KING || rank < ACE) {
			throw new InvalidRankException("invalid rank");
		}
		this.rank = rank;
	}
	
	public String toString() {
		return RANKS[rank] + " of " + SUITS[suit];
	}
	
	public boolean equals(Card other) {
		return (this.rank == other.rank) && (this.suit == other.suit);
	}
	
	public String getColor() {
		if (suit == Card.DIAMONDS || suit == Card.HEARTS)
			return Card.RED;
		return Card.BLACK;
	}

	@Override
	public int compareTo(Card arg0) {
		if (this.suit > arg0.suit) 
			return 1;
		if (this.suit == arg0.suit) {
			if (this.rank > arg0.rank)
				return 1;
			if (this.rank == arg0.rank)
				return 0;
			return -1;
		}
		return -1;
	}

}
