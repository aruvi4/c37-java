package module2;

public class Fraction {
	
	private int numerator;
	private int denominator;
	
	public Fraction () {
		numerator = 1;
		denominator = 1;
	}
	
	public Fraction (int numerator, int denominator) {
		setNumerator(numerator);
		setDenominator(denominator);
	}
	
	private static int gcd (int a, int b) {
		if (a % b == 0)
			return b;
		return gcd(b, a % b);
	}
	
	public void reduce() {
		int gcd = gcd(numerator, denominator);
		numerator /= gcd;
		denominator /= gcd;
	}

	public int getNumerator() {
		return numerator;
	}

	public void setNumerator(int numerator) {
		this.numerator = numerator;
	}

	public int getDenominator() {
		return denominator;
	}

	public void setDenominator(int denominator) {
		if (denominator == 0)
			throw new ArithmeticException ("division by zero");
		this.denominator = denominator;
	}
	
	@Override
	public String toString() {
		return numerator + "/" + denominator;
	}
	
	@Override
	public boolean equals(Object other) {
		
		if (!(other instanceof Fraction))
			return false;
		Fraction castOther = (Fraction) other;
		return castOther.numerator == this.numerator && castOther.denominator == this.denominator;
		
	}
	
	public static void main (String[] args) {
		Fraction f = new Fraction(25, 75);
		f.reduce();
		Fraction f2 = new Fraction(3, 0);
		
		Card c = new Card(Card.ACE, Card.SPADES);
		System.out.println(f.equals(c));
		
	}
	
	

}
