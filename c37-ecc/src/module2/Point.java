package module2;

public class Point {
	
	private double x;
	private double y;
	
	public Point (double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public Point() {
		this.x = 0;
		this.y = 0;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public void setX(double x) {
		this.x = x;
	}
	
	public void setY(double y) {
		this.y = y;
	}
	
	public double distance(Point other) {
		return Math.sqrt(Math.pow(other.x - this.x, 2) +
				Math.pow(other.y - this.y, 2));
	}
	
	public static double distance(Point p1, Point p2) {
		return Math.sqrt(Math.pow(p2.x - p1.x, 2) +
				Math.pow(p2.y - p1.y, 2));
	}

}
