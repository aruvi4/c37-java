package module2;

public class InvalidSuitException extends Exception {
	
	public InvalidSuitException(String message) {
		super(message);
	}

}
