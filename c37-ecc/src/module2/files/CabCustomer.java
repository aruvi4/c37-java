package module2.files;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CabCustomer {
	
	private int custId;
	private String customerName;
	private long phone;
	
	public CabCustomer() {
		
	}
	
	public CabCustomer (ResultSet rs) throws SQLException {
		this.custId = rs.getInt("cust_id");
		this.customerName = rs.getString("customer_name");
		this.phone = rs.getLong("phone");
	}

	public CabCustomer(int custId, String customerName,
			long phone) {
		super();
		this.custId = custId;
		this.customerName = customerName;
		this.phone = phone;
	}

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public long getPhone() {
		return phone;
	}

	public void setPhone(long phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "CabCustomer [custId=" + custId + ", customerName=" + customerName + ", phone=" + phone + "]";
	}
	
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof CabCustomer))
			return false;
		CabCustomer otherCabCustomer = (CabCustomer) other;
		return otherCabCustomer.phone == this.phone;
	}

}
