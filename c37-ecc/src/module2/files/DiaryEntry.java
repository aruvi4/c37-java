package module2.files;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class DiaryEntry {
	
	public static final String END_LINE = "----------";
	public static final String NEWLINE = "\n";
	private Date date;
	private String entry;
	
	public DiaryEntry(Date date, String entry) {
		this.date = date;
		this.entry = entry;
	}
	
	
	public boolean save(String filePath) throws IOException{
		//read, write, append
		
		Path file = Paths.get(filePath);
		BufferedWriter writer = Files.newBufferedWriter(file, StandardOpenOption.APPEND);
		writer.append(date.toString() + NEWLINE);
		writer.append(entry + NEWLINE);
		writer.append(END_LINE + NEWLINE);
		writer.close();
		return true;
	}
	
	public static List<DiaryEntry> readAll(String filePath) throws IOException, InvalidDateException {
		
		ArrayList<DiaryEntry> allEntries = new ArrayList<> ();
		Path file = Paths.get(filePath);
		
		BufferedReader br = Files.newBufferedReader(file);
		String line;
		
		while((line = br.readLine()) != null) {
			Date d = new Date(line);
			String e = "";
			while(!(line = br.readLine()).equals(END_LINE))
				e += line;
			DiaryEntry temp = new DiaryEntry(d, e);
			allEntries.add(temp);
		}
		return allEntries;
	}
	
	public static DiaryEntry read(String filePath, Date d) throws IOException, InvalidDateException {
		List<DiaryEntry> allEntries = readAll(filePath);
		for (DiaryEntry de : allEntries) 
			if (de.date.equals(d))
				return de;
		return null;
	}
	
	@Override
	public String toString() {
		return date + "\n" + entry;
	}

}
