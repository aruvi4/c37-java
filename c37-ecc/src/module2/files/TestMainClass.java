package module2.files;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class TestMainClass {
	
	public static void main(String[] args) {
		Path file = Paths.get("/home/aruvi/Desktop/diary.txt");
		//Path file = Paths.get("C:\\Users\\aruvi\\Desktop\\afile.txt");
		
		try {
			//Date today = new Date(11, 1, 2023);
			//DiaryEntry de = new DiaryEntry(today, "The module test is tomorrow");
			//de.save("/home/aruvi/Desktop/diary.txt");
			//List<DiaryEntry> allEntries = DiaryEntry.readAll("/home/aruvi/Desktop/diary.txt");
			//for (DiaryEntry de : allEntries)
			//	System.out.println(de);
			DiaryEntry mondays = DiaryEntry.read("/home/aruvi/Desktop/diary.txt", new Date("9/1/2023"));
			System.out.println(mondays);
		} catch (IOException | InvalidDateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// public boolean saveToFile(String filePath)
	// public static List<Employees> readAllFromFile(String filePath)
	// public static Employee readFromFile(int employeeid, String filePath)
	// refactor the Hand class to use a HashSet<Card> cards as the instance variable

}
