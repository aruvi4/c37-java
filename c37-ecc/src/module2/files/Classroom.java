package module2.files;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Classroom {
	
	private List<Student> students;
	
	public Classroom() {
		this.students = new ArrayList<> ();
	}
	
	public void addStudent(Student s) {
		this.students.add(s);
	}
	
	public Map<Integer, Student> rankList() {
		//we need to sort students
		//StudentComparator comparator = new StudentComparator();
		Collections.sort(students);
		Map<Integer, Student> rankList = new HashMap<> ();
		int rank = 1;
		for (Student s : students)
			rankList.put(rank++, s);
		return rankList;
	}
	
	public static void main(String[] args) {
		Student s1 = new Student(1, "a", 45);
		Student s2 = new Student(2, "b", 50);
		Student s3 = new Student(3, "c", 100);
		
		Classroom c = new Classroom();
		c.addStudent(s1);
		c.addStudent(s2);
		c.addStudent(s3);
		
		Map<Integer, Student> rankList = c.rankList();
		
		for (Integer i : rankList.keySet())
			System.out.println("rank " + i + ": " + rankList.get(i));
	}

}
