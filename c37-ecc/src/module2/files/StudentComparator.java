package module2.files;

import java.util.Comparator;

public class StudentComparator implements Comparator<Student>{

	@Override
	public int compare(Student arg0, Student arg1) {
		if (arg0.getMarks() > arg1.getMarks())
			return -1;
		if (arg0.getMarks() < arg1.getMarks())
			return 1;
		return 0;
	}

}
