package module2.files;

public class InvalidDateException extends Exception{
	
	public InvalidDateException(String message) {
		super(message);
	}

}
