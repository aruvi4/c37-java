package module2.files;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CharacterCounter {
	
	public static int numVowels(String filename) throws IOException{
		Path p = Paths.get(filename);
		BufferedReader reader = Files.newBufferedReader(p);
		int ascii = 0;
		int numVowels = 0;
		while ((ascii = reader.read()) != -1) {
			if ("aeiouAEIOU".contains(String.valueOf((char) ascii)))
					numVowels++;
		}
		return numVowels;
	}
	
	public static void main (String[] args) {
		String filepath = "/home/aruvi/Desktop/afile";
		try {
			System.out.println(numVowels(filepath));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
