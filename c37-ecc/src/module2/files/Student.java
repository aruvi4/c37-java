package module2.files;

public class Student implements Comparable<Student>{
	
	private int rollno;
	private String name;
	private int marks;
	
	public Student(int rollno, String name, int marks) {
		this.rollno = rollno;
		this.name = name;
		this.marks = marks;
	}
	
	public int getRollno() {
		return rollno;
	}
	public void setRollno(int rollno) {
		this.rollno = rollno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMarks() {
		return marks;
	}
	public void setMarks(int marks) {
		this.marks = marks;
	}
	@Override
	public String toString() {
		return "Student [rollno=" + rollno + ", name=" + name + ", marks=" + marks + "]";
	}
	
	@Override
	public boolean equals(Object other) {
		if (! (other instanceof Student))
			return false;
		Student otherStudent = (Student) other;
		return otherStudent.rollno == this.rollno;
	}

	@Override
	public int compareTo(Student arg0) {
		if (this.marks > arg0.marks)
			return -1;
		if (this.marks < arg0.marks)
			return 1;
		return 0;
	}

}
