package module2;
import module2.exceptions.InsufficientBalanceException;

public class SavingsAccount extends Account {
	
	public SavingsAccount(String name, String address, double balance) {
		super(name, address, balance);
	}
	
	public void withdraw(double amount) throws InsufficientBalanceException{
		if (amount > balance) {
			throw new InsufficientBalanceException("not enough balance");
		}
		amount -= balance;
	}

}
