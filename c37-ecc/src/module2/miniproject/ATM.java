package module2.miniproject;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import module2.miniproject.exceptions.AccountDoesNotExist;

public class ATM {
	
	private AccountManager amgr;
	
	public ATM() {
		amgr = new AccountManager();
	}

	
	public static boolean verifyPin (Account acc, int enteredPin) {
		return acc.getPin() == enteredPin;
	}
	
	public void changePin (Account acc) throws SQLException {
		System.out.println("enter new PIN");
		Scanner sc = new Scanner(System.in);
		int newPin = sc.nextInt();
		acc.setPin(newPin);
		amgr.update(acc);
	}
	
	public void withdraw (Account acc) throws SQLException {
		System.out.println("How much money do you want to withdraw? ");
		Scanner sc = new Scanner(System.in);
		double amount = sc.nextDouble();
		if (amgr.withdraw(acc, amount))
			System.out.println("Please collect " + amount + "from the cash section ;)");
		else 
			System.out.println("Not enough balance!");
	}
	
	public void deposit (Account acc) throws SQLException {
		System.out.println("How much money do you want to deposit? ");
		Scanner sc = new Scanner(System.in);
		double amount = sc.nextDouble();
		if (amgr.deposit(acc, amount))
			System.out.println("Deposit successful!");
	}
	
	public int selectOption() {
		int option = 0;
		while (true) {
			Scanner sc = new Scanner(System.in);
			System.out.println("Choose an option: ");
			System.out.println("1. Change PIN");
			System.out.println("2. Withdraw");
			System.out.println("3. Deposit");
			option = sc.nextInt();
			if (option == 1 || option == 2 || option == 3)
				break;
		}
		return option;
	}
	
	public static void main(String[] args) {
		ATM atm = new ATM();
		Scanner sc = new Scanner(System.in);
		while (true) {
			System.out.println("Welcome to the ATM, 1 to search by id, 2 to search by name, press 0 to exit");
			int searchOption = sc.nextInt();
			if (searchOption == 0)
				break;
			try {
				Account acc = null;
				if (searchOption == 1) {
					System.out.println("Enter account number");
					int accountNumber = sc.nextInt();
					acc = atm.amgr.getAccount(accountNumber);
				}
				else if (searchOption == 2) {
					System.out.println("Enter name");
					String name = sc.next();
					List<Account> accounts = atm.amgr.searchByName(name);
					for (Account a : accounts) {
						System.out.println(a);
						System.out.println("If this is your account, type 'y' or else 'n' to see more results");
						String choice = sc.next();
						if (choice.equalsIgnoreCase("y")) {
							acc = a;
							break;
						}
					}
				}
				if (acc == null)
					break;
				System.out.println("Your account details are: ");
				System.out.println(acc);
				int option = atm.selectOption();
				System.out.println("Enter your PIN");
				int numPinAttempts = 0;
				while (numPinAttempts < 3) {
					int enteredPin = sc.nextInt();
					if (!verifyPin(acc, enteredPin)) {
						System.out.println("enter correct PIN (" + numPinAttempts + "attempts remaining)");
						numPinAttempts++;
						continue;
					}
					if (option == 1) {
						atm.changePin(acc);
						break;
					}
					if (option == 2) {
						atm.withdraw(acc);
						break;
					}
					if (option == 3) {
						atm.deposit(acc);
						break;
					}
				}
				System.out.println("Your account details after modification are: ");
				System.out.println(acc);
			} 
			catch (AccountDoesNotExist e) {
				System.out.println("No account with that number");
				continue;
			}
			catch (SQLException e) {
				e.printStackTrace();
				continue;
			}
		}
		
	}

}
