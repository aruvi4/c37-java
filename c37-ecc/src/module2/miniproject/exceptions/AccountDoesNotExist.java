package module2.miniproject.exceptions;

public class AccountDoesNotExist extends Exception{
	
	public AccountDoesNotExist(String message) {
		super(message);
	}

}
