package module2.miniproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import module2.miniproject.exceptions.AccountDoesNotExist;

public class AccountManager {
	
	private Connection cnx;
	
	private static final String CREATE_ACCOUNT = "insert into account (name, pin, balance) values (?, ?, ?)";
	private static final String SEARCH_BY_ID = "select * from account where account_number = ?";
	private static final String UPDATE_ACCOUNT = "update account set account_number = ?, name = ?, pin = ?, balance = ? where account_number = ?";
	private static final String SEARCH_BY_NAME = "select * from account where name = ?";
	
	public void openConnection () throws SQLException {
		if (cnx == null || cnx.isClosed())
			cnx = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb",
					"javauser",
					"javapassword");	
	}
	
	public void closeConnection() throws SQLException {
		cnx.close();
	}
	
	public Account createAccount(Account acc) throws SQLException {
		openConnection();
		PreparedStatement pstmt = cnx.prepareStatement(CREATE_ACCOUNT, Statement.RETURN_GENERATED_KEYS);
		pstmt.clearParameters();
		pstmt.setString(1, acc.getName());
		pstmt.setInt(2, acc.getPin());
		pstmt.setDouble(3, acc.getBalance());
		pstmt.execute();
		ResultSet rs = pstmt.getGeneratedKeys();
		if (rs == null)
			//exception logic
			return null;
		rs.next();
		acc.setAccountNumber(rs.getInt(1));
		closeConnection();
		return acc;
	}
	
	public Account getAccount(int accountNumber) throws SQLException, AccountDoesNotExist{
		openConnection();
		PreparedStatement pstmt = cnx.prepareStatement(SEARCH_BY_ID);
		pstmt.clearParameters();
		pstmt.setInt(1, accountNumber);
		ResultSet rs = pstmt.executeQuery();
		if (!rs.next())
			throw new AccountDoesNotExist("no account with that ID");
		Account acc = new Account(rs.getInt("account_number"), 
				rs.getString("name"), rs.getInt("pin"), rs.getDouble("balance"));
		closeConnection();
		return acc;
	}
	
	public List<Account> searchByName(String name) throws SQLException, AccountDoesNotExist {
		openConnection();
		PreparedStatement pstmt = cnx.prepareStatement(SEARCH_BY_NAME);
		pstmt.clearParameters();
		pstmt.setString(1, name);
		ResultSet rs = pstmt.executeQuery();
		ArrayList<Account> accounts = new ArrayList<Account> ();
		while (rs.next())
			accounts.add(new Account(rs.getInt("account_number"), 
				rs.getString("name"), rs.getInt("pin"), rs.getDouble("balance")));
		closeConnection();
		return accounts;
	}
	
	public boolean update(Account acc) throws SQLException{
		openConnection();
		PreparedStatement pstmt = cnx.prepareStatement(UPDATE_ACCOUNT);
		pstmt.clearParameters();
		pstmt.setInt(1, acc.getAccountNumber());
		pstmt.setString(2, acc.getName());
		pstmt.setInt(3, acc.getPin());
		pstmt.setDouble(4, acc.getBalance());
		pstmt.setInt(5, acc.getAccountNumber());
		return (pstmt.executeUpdate() > 0);
	}
	
	public boolean withdraw(Account acc, double amount) throws SQLException{
		if (acc.getBalance() < amount)
			return false;
			//exception logic
		acc.setBalance(acc.getBalance() - amount);
		return update(acc);
	}
	
	public boolean deposit(Account acc, double amount) throws SQLException {
		acc.setBalance(acc.getBalance() + amount);
		return update(acc);
	}

}
