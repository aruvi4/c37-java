package module2;

public class Adder {
	
	public int add(int a, int b) {
		return a + b;
	}
	
	public int add(int a, int b, int c) {
		return a + b + c;
	}
	
	public static void main(String[] args) {
		Adder a = new Adder();
		System.out.println(a.add(5, 10));
		System.out.println(a.add(5, 10, 15));
	}

}
