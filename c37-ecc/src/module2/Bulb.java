package module2;

public class Bulb {
	
	private boolean isSwitchedOn = false;
	private String colour = "";
	
	public Bulb() {
		isSwitchedOn = false;
		colour = "White";
	}
	
	public Bulb(String myColour) {
		colour = myColour;
	}
	
	public void setColour(String myColour) {
		colour = myColour;
	}
	
	public String getColour() {
		return colour;
	}
	
	public void switchOn() {
		isSwitchedOn = true;
	}
	
	public void switchOff() {
		isSwitchedOn = false;
	}
	
	public boolean getSwitchedOn() {
		return isSwitchedOn;
	}
	
	//@Override
	public String toString() {
		String state = (isSwitchedOn) ? "on" : "off";
		return "Bulb colour " + colour + " is switched " + state;
	}
	
	public static void main (String[] args) {
		Bulb b = new Bulb();
		System.out.println(b);
	}

}
