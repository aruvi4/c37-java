package module2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Hand {
	
	private List<Card> cards;
	
	public Hand() {
		cards = new ArrayList<> ();
	}
	
	public void add(Card c) {
		if (!(cards.contains(c)))
			cards.add(c);
	}
	
	public void remove(Card c) {
		if (cards.contains(c))
			cards.remove(c);
	}
	
	public void sort() {
		Collections.sort(cards);
	}

}
