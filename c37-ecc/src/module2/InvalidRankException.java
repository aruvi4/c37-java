package module2;

public class InvalidRankException extends Exception{
	
	public InvalidRankException(String message) {
		super(message);
	}

}
