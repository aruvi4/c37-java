package module2;

public class TalentSprintStudent extends Student{
	
	private String enrolmentId;
	
	public TalentSprintStudent(String name, String gender, String enrolmentId) {
		super(name, 0, gender, 0, 0);
		this.enrolmentId = enrolmentId;
	}
	
	public String getEnrolmentId() {
		return enrolmentId;
	}

}
