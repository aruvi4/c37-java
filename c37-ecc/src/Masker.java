
public class Masker {
	
	public static String maskAll(String input) {
		String out = "";
		for(int i = 0; i < input.length(); i++) 
			if (i < 2)
				out += input.charAt(i);
			else
				out += "X";
		return out;
	}
	
	public static String mask(String email) {
		String[] parts = email.split("@");
		//test@abc.def -> "test", "abc.def"
		//"i like red apples".split(" ") -> "i", "like", "red", "apples" 
		return maskAll(parts[0]) + "@" + parts[1];
	}
	
	public static String alternateMask(String email) {
		String out = "";
		for (int i = 0; i < email.length(); i++) {
			if (i < 2 || i >= email.indexOf('@'))
				out += email.charAt(i);
			else
				out += "X";
		}
		return out;
	}
	
	public static int numVowels(String input) {
		int count = 0;
		String vowels = "aeiouAEIOU";
		for (int i = 0; i < input.length(); i++) {
			//String c = String.valueOf(input.charAt(i));
			String c = "" + input.charAt(i);
			if (vowels.contains(c))
				count++;
		}
		return count;
	}
	
	public static void main(String[] args) {
		System.out.println(numVowels("str"));
	}

}
