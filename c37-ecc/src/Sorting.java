
public class Sorting {
	
	public static boolean isSortedAsc (int arr[]) {
		for (int i = 1; i < arr.length; i++)
			if (arr[i] < arr[i - 1])
				return false;
		return true;
	}
	
	public static void swap (int arr[], int i, int j) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}
	
	public static int minIndexFrom (int arr[], int i) {
		int minIndex = i;
		for (int j = i; j < arr.length; j++)
			if (arr[j] < arr[minIndex])
				minIndex = j;
		return minIndex;
	}
	
	public static void selectionSort(int arr[]) {
		for (int i = 0; i < arr.length; i++)
			swap(arr, i, minIndexFrom(arr, i));
	}
	
	public static void main (String[] args) {
		int arr1[] = {60, 30, 20, 50, 100, 80, 25, 120};
//		swap (arr1, 5, 7);
		selectionSort(arr1);
		for (int i = 0; i < arr1.length; i++)
			System.out.print(arr1[i] + " ");
		
	}

}
