
public class InterestingNumbers {
	
	public static final String SEPARATOR = ", ";
	
	public static long factorial (int num) {
		if (num <= 1)
			return 1;
		return num * factorial (num - 1);
	}
	
	public static boolean isStrong(int num) {
		int copy = num;
		int factorialSumDigits = 0;
		while (copy > 0) {
			factorialSumDigits += factorial (copy % 10);
			copy /= 10;
		}
		return factorialSumDigits == num;
	}
	
	public static String getStrong (int limit) {
		String strongNums = "";
		for (int i = 0; i < limit; i++)
			if (isStrong(i))
				strongNums += i + SEPARATOR;
		return strongNums.substring(0, strongNums.length() - 2);		
	}
	
	public static String printNums (int arr[]) {
		String output = "";
		for (int i = 0; i < arr.length; i++) {
			output += arr[i] + SEPARATOR;
		}
		return output;
	}
	
	public static int max(int arr[]) {
		int biggestSoFar = arr[0];
		for (int i = 0; i < arr.length; i++) 
			if (arr[i] > biggestSoFar)
				biggestSoFar = arr[i];
		return biggestSoFar;
	}
	
	public static int maxAlt(int arr[]) {
		int biggestSoFar = arr[0];
		for (int item : arr)
			if (item > biggestSoFar)
				biggestSoFar = item;
		return biggestSoFar;
	}
	
	public static int sum(int arr[]) {
		int sum = 0;
		for (int item: arr)
			sum += item;
		return sum;
	}
	
	public static int numZeroes (int arr[]) {
		int numZeroes = 0;
		for (int item : arr)
			if (item == 0)
				numZeroes++;
		return numZeroes;
	}
	
	public static void main (String[] args) {
		System.out.println(getStrong(41000));
		int a[] = new int[10];
		a[0] = 25;
		a[9] = 99;
		int b[] = {25, 10, 4, 26, 15, 33, 2, -5, -19};
		System.out.println(printNums(a));
	}
	

}
