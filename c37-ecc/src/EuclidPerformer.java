
public class EuclidPerformer {
	
	public static boolean isPrime(int num) {
		int divisor = 2;
		while (divisor * divisor <= num) {
			if(num % divisor == 0)
				return false;
			divisor++;
		}
		return true;
	}

	public static int gcd (int num1, int num2) {
		if(num1 % num2 == 0) {
			return num2;
		}
		return gcd(num2, num1 % num2);
	}
	
	public static boolean are_coprime (int num1, int num2) {
		return gcd(num1, num2) == 1;
	}
	
	public static long factorial (long num) {
		//your recursive code here
		//every recursive method has 2 components
		//1. The exit condition
		//2. Modify variable and recursive call.
		if (num == 1)
			return 1;
		return num * factorial(num - 1);
	}
	
	public static long nthFibonacci(int n) {
		if (n == 1)
			return 0;
		if (n == 2)
			return 1;
		return nthFibonacci(n - 1) + nthFibonacci(n - 2);
	}
	
	public static int numDigits(int num) {
		if (num < 10)
			return 1;
		return 1 + numDigits(num / 10);
	}
	
	public static int sumDigits(int num) {
		if (num < 10)
			return num;
		return num % 10 + sumDigits(num / 10);
	}
	
	public static int sumDigitsLoop (int num) {
		int sum = 0;
		while (num > 0) {
			sum += num % 10;
			num /= 10;
		}
		return sum;
	}
	
	public static long pow(long base, long exp) {
		if (exp <= 0)
			return 1;
		return base * pow(base, exp - 1);
	}
	
	public static long sumUpto(long limit) {
		if (limit <= 0)
			return 0;
		return limit + sumUpto(limit - 1);
	}
	
	public static void main(String[] args) {
		System.out.println(sumUpto(pow(2, nthFibonacci(5))));
	}

}
